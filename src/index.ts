
import BattleSimulation from "./BattleSimulation";
import {Hero} from "./characters/Heros";
import {Assassin} from "./characters/monsters/Assassin";
import { Berserker } from "./characters/monsters/Berserker";
import { Dragon } from "./characters/monsters/Dragon";
import { Golem } from "./characters/monsters/Golem";
import { Griffin } from "./characters/monsters/Griffin";
import { Werewolf } from "./characters/monsters/Werewolf";

// Hero
export let hero = new Hero("Nicolas", 100, 30, 0, "human");
console.log(hero, hero.getHealth(), hero.getHitStrength(), hero.getXp(), hero.getRace());

// Assassin
export let assassin = new Assassin("Meurtrier Noir", 100, 10, 1);
console.log(assassin, assassin.getHealth(), assassin.getHitStrength(), assassin.getXp());

// Berzerker
export let berserker = new Berserker("Berserker", 100, 10, 1);
console.log(berserker, berserker.getHealth(), berserker.getHitStrength(), berserker.getXp());

// Dragon
export let dragon = new Dragon("Dragon", 100, 10, 1);
console.log(dragon, dragon.getHealth(), dragon.getHitStrength(), dragon.getXp());

// Golem
export let golem = new Golem("Golem", 100, 10, 1);
console.log(golem, golem.getHealth(), golem.getHitStrength(), golem.getXp());

// Griffin
export let griffin = new Griffin("Griffon", 100, 10, 1);
console.log(griffin, griffin.getHealth(), griffin.getHitStrength(), griffin.getXp());

// Werewolf
export let werewolf = new Werewolf("Loup Garou", 100, 10, 1);
console.log(werewolf, werewolf.getHealth(), werewolf.getHitStrength(), werewolf.getXp());

// Lancement des combats
new BattleSimulation();


// const title = 'Hello TypeScript' as string;

// document.getElementById('content')!.innerHTML = `${title}`;

// console.log(title, stringYoYo(title));

// /**
//  * Convertit une lettre sur 2 en majuscule
//  * @param title 
//  */
// function stringYoYo(title: string): string
// {
//     const arr = title.split('');

//     const newArr = arr.map((letter, index) => {
//         if (index % 2 == 0) 
//         {
//             return letter.toUpperCase();
//         }
        
//         return letter;
        
//     });

//     return newArr.join('');
// }
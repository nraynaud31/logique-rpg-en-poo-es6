import { assassin, berserker, dragon, golem, griffin, hero, werewolf } from ".";
import Battle from "./Battle";

export default class BattleSimulation extends Battle{

    /**
     * Enchaîner les combats
     */

    constructor() 
    {
        super();
        
        this.fight(hero, griffin);
        this.fight(hero, berserker);
        this.fight(hero, dragon);
        this.fight(hero, assassin);
        this.fight(hero, golem);
        this.fight(hero, werewolf);
    }
}
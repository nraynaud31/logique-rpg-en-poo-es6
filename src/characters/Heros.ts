import Character from "./Character";
import { Dragon } from "./monsters/Dragon";
import { Griffin } from "./monsters/Griffin";
export class Hero extends Character
{
    /**
     * Race du Character
     */
    race:string

    /**
     * On créé le Heros
     * @param name Nom du Heros
     * @param health Vie du Heros
     * @param hitStrength Points de force du Heros
     * @param xp Points d'expériences du Heros
     * @param race Race du Heros
     */
    constructor(name: string, health: number, hitStrength: number, xp: number, race: string)
    {
        super(name, health, hitStrength, xp)
        this.race = race
    }

    /**
     * Lvl du Héros
     * @param level Lvl du Héros 
     */
    public setLvl(level: number): void
    {
        if (this.xp == 10)
        {
            this.lvl += level
            this.xp = 0
        }
    }

    /**
     * Points d'expériences du Heros
     * @param xp Points d'expériences du Héros
     */

    public setXp(xp: number): void
    {
        this.xp += 2
    }

    /**
     * On récupère la race du Héros
     * @returns Race du Héros
     */
    
    getRace()
    {
        return this.race
    }

    /**
     * Vie du Héros
     * @param damage Dégâts reçus
     */

    public setHealth(damage: number): void 
    {
        if(this.race == "dwarf")
        {
            let damageDiminu = Math.floor(Math.random() * 100)+1;
            
            if(damageDiminu <= 20)
            {
                this.health -= (damage*0.5)
                console.log("Le Héro nain a réussi à bloquer 50% des dégats reçus.");
                
            }
            
            else
            {
                this.health -= damage
            }
        }
        
        else if(this.race == "elf")
        {
            this.health -= damage;
        }

        else if (this.race == "human")
        {
            this.health -= damage;
        }
    }

    /**
     * Attaque du Héros
     * @param monster Monstre
     * @returns Points de forces * le lvl en fonction des races
     */
    attack(monster: Character)
    {
        if(this.race == "dwarf")
        {
            return this.hitStrength * this.lvl
        }

        else if(this.race == "elf")
        {
            if(monster instanceof Dragon || monster instanceof Griffin)
            {
                if(monster.attackFly == "AtFFl")
                {
                    console.log("Votre attaque est réduite de 10% car le Dragon ou le Griffon est au sol.")
                    return (this.hitStrength * this.lvl) * 0.9
                }
                else if (monster.attackFly == "fly" || monster.attackFly == "AtFSk")
                {
                    console.log("Votre attaque est augmenté de 10% car le Dragon ou le Griffon est dans les airs.");
                    return (this.hitStrength * this.lvl) * 1.1
                }
            }
            else
            {
                console.log("Votre attaque est réduite de 10% car votre ennemie uniquement terrestre.");
                return (this.hitStrength * this.lvl) * 0.9
            }
        }

        else if (this.race == "human")
        {
            if(monster instanceof Dragon || monster instanceof Griffin)
            {
                if(monster.attackFly == "AtFFl")
                {
                    console.log("Votre attaque est augmenté de 10% car le Dragon ou le Griffon est au sol.")
                    return (this.hitStrength * this.lvl) * 1.1
                }
                else if (monster.attackFly == "fly" || monster.attackFly == "AtFSk")
                {
                    console.log("Votre attaque est réduite de 10% car le Dragon ou le Griffon est dans les airs.");
                    return (this.hitStrength * this.lvl) * 0.9
                }
            }
            else
            {
                console.log("Votre attaque est augmenté de 10% car votre ennemie uniquement terrestre.");
                return (this.hitStrength * this.lvl) * 1.1
            }
        }
    }
}
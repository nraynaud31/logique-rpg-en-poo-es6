import Character from "../Character";

export class Werewolf extends Character
{
    /**
     * On créé le Werewolf
     * @param name Nom du Werewolf
     * @param health Vie du Werewolf
     * @param hitStrength Points de force du Werewolf
     * @param xp Points d'expériences du Werewolf
     */
    super(name: string, health: number, hitStrength: number, xp: number){}

    /**
     * Vie du Werewolf
     * @param damage Dégâts reçus
     */
    public setHealth(damage: number): void
    {
        this.health -= (damage*0.5)
        console.log("Le loup garou réduit les dégats reçus de 50%.")
    }
}
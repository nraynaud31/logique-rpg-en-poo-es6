import Character from "../Character";

let attaqueA = 0
export class Assassin extends Character
{

    /**
     * Créé l'assassin
     * @param name Nom de l'assasin
     * @param health Vie de l'assassin
     * @param hitStrength Point de force de l'assassin
     * @param xp Points d'expériences de l'assassin
     */
    super(name: string, health: number, hitStrength: number, xp: number){}

    // setHitStrength(strength: number): void {
    //     this.hitStrength += (10/100*this.hitStrength);
    // }

    /**
     * Permet d'augmenter les dégâts de l'assassin
     * @returns Augmentation des dégâts de 10%
     */
    public attack() {
        this.hitStrength *= 1.1
        console.log("l'attaque de l'assassin augmente de 10%.");
        return (this.hitStrength * this.lvl)
        
        
    }
}
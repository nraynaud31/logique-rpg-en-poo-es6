import Character from "../Character";

export class Berserker extends Character
{

    /**
     * On créé le Berzerker
     * @param name Nom du Berzerker
     * @param health Vie du Berzerker
     * @param hitStrength Point de force du Berzerker
     * @param xp Points d'expérience du Berzerker
     */

    constructor(name: string, health: number, hitStrength: number, xp: number)
    {
        super(name, health, hitStrength, xp)
    }
    /**
     * Définir la vie du Berzerker
     * @param damage Dégâts reçus 
     */

    public setHealth(damage: number): void
    {
        this.health -= (damage*0.7)
        console.log("Le Berserker réduit les dégats reçus de 30%.")
    }
}
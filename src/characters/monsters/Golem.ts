import Character from "../Character";


export class Golem extends Character
{
    /**
     * On créé le Golem
     * @param name Nom du Golem
     * @param health Vie du Golem
     * @param hitStrength Points de force du Golem
     * @param xp Points d'expériences du Golem
     */
    super(name: string, health: number, hitStrength: number, xp: number){}

    /**
     * Vie du Golem
     * @param damage Dégâts reçus
     */
    public setHealth(damage: number): void
    {
        let invulnerabilitie = Math.floor(Math.random() * 100)+1;

        if(invulnerabilitie <= 50)
        {
            this.health -= 0;
            console.log("La solide peau du golem annule les dégats reçus.")
        }
        else
        {
            this.health -= damage;
        }
    }
}
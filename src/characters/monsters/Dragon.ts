import Character from "../Character";

export class Dragon extends Character
{
    /**
     * On définit un paramètre pour attaquer en volant
     */
    public attackFly:string = "AtFFl";
    
    /**
     * On créé le Dragon
     * @param name Nom du Dragon
     * @param health Vie du Dragon
     * @param hitStrength Point de force du Dragon
     * @param xp Points d'expériences du Dragon
     */
    
    constructor(name: string, health: number, hitStrength: number, xp: number)
    {
        super(name, health, hitStrength, xp,)
    }

    /**
     * On définit la vie du Dragon
     * @param damage Dégâts reçus
     */
    
    public setHealth(damage: number): void
    {
        if(this.attackFly == "AtFFl")
        {
            this.health -= (damage*0.5)
            console.log("Le Dragon réduit les dégats reçus de 50%.")


        }else if(this.attackFly == "fly")
        {
            this.health -= (damage*0.4)
            console.log("Le Dragon réduit les dégats reçus de 60%.")

        }else if(this.attackFly == "AtFSk")
        {
            this.health -= (damage*0.4)
            console.log("Le Dragon réduit les dégats reçus de 60%.")
        }
    }

    /**
     * Attaque du Dragon
     * @returns Puissance d'attaquer en fonction de sa situation (vol, attaque depuis le ciel, ou attaque au sol)
     */

    public attack(): number {
        if(this.attackFly == "AtFFl")
        {
            this.attackFly = "fly"
            console.log("Le Dragon se pose au sol et utilise sa puissance normale.")
            return this.hitStrength * this.lvl
            
        }else if(this.attackFly == "fly")
        {
            this.attackFly = "AtFSk"
            console.log("Le Dragon s'envole et augmente la puissance de son attaque de 10%. ne feras pas de dégats lors de ce tours.")
            return (this.hitStrength * this.lvl) * 0;
            
        }else if(this.attackFly == "AtFSk")
        {
            this.attackFly = "AtFFl"
            console.log("La puissance d'attaque du Dragon est augmenté de 10%.")
            return (this.hitStrength * this.lvl) * 1.1;
        }        
    }
}
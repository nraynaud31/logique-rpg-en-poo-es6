import Character from "../Character";


export class Griffin extends Character
{
    /**
     * Paramètre d'attaque en volant
     */
    public attackFly:string = "AtFFl"


    /**
     * On créé le Griffin
     * @param name Nom du Griffin
     * @param health Vie du Griffin
     * @param hitStrength Points de force du Griffin
     * @param xp Points d'expériences du Griffin
     */
    constructor(name: string, health: number, hitStrength: number, xp: number)
    {
        super(name, health, hitStrength, xp,)
    }

    /**
     * Vie du Griffin
     * @param damage Dégâts reçus
     */    
    public setHealth(damage: number): void
    {
        if(this.attackFly == "AtFFl")
        {
            this.health -= damage

        }else if(this.attackFly == "fly")
        {
            this.health -= (damage*0.9)
            console.log("Le Griffon réduit les dégats reçus de 10%.")


        }else if(this.attackFly == "AtFSk")
        {
            this.health -= (damage*0.9)
            console.log("Le Griffon réduit les dégats reçus de 10%.")
        }
    }

    /**
     * Attaque du Griffin
     * @returns Puissance d'attaquer en fonction de sa situation (vol, attaque depuis le ciel, ou attaque au sol)
     */

    public attack(): number {
        if(this.attackFly == "AtFFl")
        {
            this.attackFly = "fly"
            console.log("Le Griffon se pose au sol et utilise sa puissance normale.")
            return this.hitStrength * this.lvl
            
        }else if(this.attackFly == "fly")
        {
            this.attackFly = "AtFSk"
            console.log("Le Griffon s'envol et augmente la puissance de son attaque de 10%. Il ne feras pas de dégat lors de ce tours.")
            return (this.hitStrength * this.lvl) * 0; 
            
        }else if(this.attackFly == "AtFSk")
        {
            this.attackFly = "AtFFl"
            console.log("La puissance d'attaque du Griffon est aurmenter de 10%.")
            return (this.hitStrength * this.lvl) * 1.1;
        }        
    }
}
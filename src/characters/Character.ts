let attaque = 0
export default class Character {
    /**
     * On créé nos paramètres
     */
    name:string;
    health:number;
    hitStrength: number;
    lvl:number = 1;
    xp:number;
    

    /**
     * On créé un Character
     * @param name Nom du Character
     * @param health Vie du Character
     * @param hitStrength Points de force du Character
     * @param xp Points d'expériences du Character
     */
    constructor(name: string, health: number, hitStrength: number, xp: number)
    {
        this.name = name;
        this.health = health;
        this.hitStrength = hitStrength;
        this.xp = xp;
    }

    /**
     * Récupérer le nom du Character
     * @returns Nom du Character
     */

    public getName()
    {
        return this.name;
    }

    /**
     * Définir un nom au Character
     * @param name Nom du Character
     */

    public setName(name: string)
    {
        this.name = name
    }

    /**
     * Récupérer la vie du Character
     * @returns Vie du Character
     */

    public getHealth() {
        return this.health;
    }

    /**
     * Définir la vie du Character
     * @param damage Nouvelle vie du Character
     */

    public setHealth(damage: number)
    {
        this.health = this.health - damage;
    }

    /**
     * Récupérer le Lvl du Character
     * @returns Lvl du Character
     */

    public getLvl()
    {
        return this.lvl
    }

    /**
     * Définir le Lvl du Character
     * @param level Lvl du Character
     */

    public setLvl(level: number)
    {
        this.lvl = level;
    }

    /**
     * Récupérer les points d'expériences du Character
     * @returns Points d'expériences du Character
     */

    public getXp()
    {
        return this.xp;
    }

    /**
     * Définir les points d'expériences du Character
     * @param xp Points d'expériences du Character
     */

    public setXp(xp: number)
    {
        this.xp = xp;
    }

    /**
     * Récupérer les points de forces du Character
     * @returns Points de forces du Character
     */

    public getHitStrength()
    {
        return this.getHitStrength;
    }

    /**
     * Définir les points de forces du Character
     * @param strength Points de forces du Character
     */

    public setHitStrength(strength: number)
    {
        this.hitStrength = strength;
    }

    /**
     * Permettre au Character D'attaquer
     * @param monster Monstre
     * @returns Points de force * le lvl
     */

    public attack(monster?: Character)
    {
        
        return this.hitStrength * this.lvl;
    }

    /**
     * Mort du Character
     */

    public die()
    {

    }
}

